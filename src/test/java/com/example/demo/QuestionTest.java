package com.example.demo;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class QuestionTest {

	@Test
	void testAnswers() {
		assertEquals(54, Question.ask("How old is David Cameron"));
		assertEquals(67, Question.ask("How old is Tony Blair"));
		assertEquals("Edinburgh",Question.ask("What is the birth place name of Tony Blair"));
	}

}


