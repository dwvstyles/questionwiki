package com.example.demo.rest;

import com.example.demo.data.WikiData;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import java.util.Collections;

public class RestHandler {

    public static WikiData query(String url, String innerQuery) {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity request = new HttpEntity(headers);

        ResponseEntity<WikiData> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                request,
                WikiData.class,
                innerQuery
        );

        if (response.getStatusCode() == HttpStatus.OK) {
            System.out.println(" Request Successful : " + response.getStatusCode());
            //System.out.println(response.getBody());
        } else {
            System.out.println("Request Failed : " + response.getStatusCode());
            throw new RuntimeException("Request Failed. Fail tidily please.");
        }

        return response.getBody();
    }
}
