package com.example.demo;

import com.example.demo.data.WikiData;
import com.example.demo.rest.RestHandler;
import com.example.demo.utils.DateAndAgeUtil;

public class Question {

    private static final String URL = "https://query.wikidata.org/sparql?query={id}";
    private static final String AGE_QUERY = "select ?value where {?z rdfs:label \"THE_NAME\" @en. ?z wdt:P569 ?value.}";
    private static final String BIRTH_PLACE_QUERY = "select ?value where {?z rdfs:label \"THE_NAME\" @en. ?z wdt:P19 ?a. ?a wdt:P1448 ?value.}";

    public static Object ask(String question) {

        if (question.startsWith("What")) {
            return findBirthplace(question);
        } else if (question.startsWith("How")) {
            return findHowOld(question);
        }

        return "Invalid Question";

    }


    private static Object findHowOld(String question) {

        String who = question.substring(11);

        String innerQuery = AGE_QUERY.replace("THE_NAME", who);

        WikiData data = RestHandler.query(URL, innerQuery);

        return DateAndAgeUtil.agefromBirthday(data.getValue());

    }

    private static Object findBirthplace(String question) {

        String who = question.substring(32);

        String innerQuery = BIRTH_PLACE_QUERY.replace("THE_NAME", who);
        WikiData data = RestHandler.query(URL, innerQuery);

        return data.getEnglishTextValue();

    }

}
