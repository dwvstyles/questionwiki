package com.example.demo.data;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;


public class WikiData {
    public Head head;
    public Results results;

    public String getValue() {
        return results.bindings.get(0).value.value;
    }

    public String getEnglishTextValue() {
        Binding b = results.bindings.stream()
                .filter(v -> v.value.xmlLang.equals("en"))
                .collect(Collectors.toList())
                .get(0);

        return b.value.value;

    }

}

class Binding {
    public Value value;
}

class Results {
    public List<Binding> bindings;
}

class Head {
    public List<String> vars;
}

class Value {
    @JsonProperty("xml:lang")
    public String xmlLang;
    public String datatype;
    public String type;
    public String value;
}










