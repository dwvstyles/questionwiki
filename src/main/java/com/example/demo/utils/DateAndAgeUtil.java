package com.example.demo.utils;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DateAndAgeUtil {

    public static int agefromBirthday(String birthdate) {

        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
        LocalDate date = LocalDate.parse(birthdate, inputFormatter);
        LocalDate now = LocalDate.now(ZoneOffset.UTC);

        return Period.between(date, now).getYears();
    }


}
